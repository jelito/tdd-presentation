<?php
define('PHPUNIT_PROJECT_DIR', dirname(dirname(__FILE__)));
define('PHPUNIT_TESTS_DIR', dirname(__FILE__));
define('UNIT_TESTS_RUN', 1);
date_default_timezone_set("Europe/Prague");

error_reporting(E_ALL);

require_once PHPUNIT_TESTS_DIR . '/BaseTestCase.php';
require_once PHPUNIT_TESTS_DIR . '/DevStack/Mocker/Mocker.php';
require_once PHPUNIT_TESTS_DIR . '/DevStack/ReflectionHelper.php';

